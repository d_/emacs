;; -*- lexical-binding: t; -*-

;; prevent package.el loading packages
(setq package-enable-at-startup nil)

;; defer garbage collect until after startup
(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.6)

;; unset `file-name-handler-alist` until startup
;; this is used for opening remote/compressed/encrypted/etc. files 
;; which is probably not needed for startup
(defvar saved-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)


(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold (* 16 1024 1024)
                  gc-cons-percentage 0.1
                  file-name-handler-alist saved-file-name-handler-alist)))
